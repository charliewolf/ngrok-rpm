Name: ngrok
Version: 3.6.0
Release:    1%{?dist}
Summary: ngrok
ExclusiveArch: x86_64 aarch64

Group: Devlopment/Tools
License: Special Proprietary
%ifarch x86_64
Source0: https://bin.equinox.io/c/bNyj1mQVY4c/%{name}-v3-%{version}-linux-amd64.tgz
%elifarch aarch64
Source0: https://bin.equinox.io/c/bNyj1mQVY4c/%{name}-v3-%{version}-linux-arm64.tgz
%endif
Source1: %{name}@.service
Source2: %{name}.sysusers

BuildRequires: systemd-rpm-macros

%description
This package contains ngrok

%prep
%setup -c

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}/usr/lib/sysusers.d
install -m 755 ngrok %{buildroot}%{_bindir}/ngrok
cp %{SOURCE1} %{buildroot}%{_unitdir}/%{name}@.service
cp %{SOURCE2} %{buildroot}/usr/lib/sysusers.d/%{name}.conf

%post
systemd-sysusers

%files
%{_bindir}/ngrok
%{_unitdir}/%{name}@.service
/usr/lib/sysusers.d/%{name}.conf
