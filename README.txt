A basic RPM package for ngrok.

You can define your tunnels in /etc/ngrok.yml and then use systemd to run them. For example:

/etc/ngrok.yml
===================
authtoken: token  
tunnels:
    demo:
      proto: http
      addr: 9090

starting the tunnel
======================
systemctl enable --now ngrok@demo
